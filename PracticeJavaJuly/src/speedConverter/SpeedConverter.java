package speedConverter;

public class SpeedConverter {
    public static long toMilesPerHour(double kmperHour){
        if(kmperHour < 0){
            return -1;
        }
        long milesPerHour = Math.round(kmperHour/1.609);
        return milesPerHour;
    }
}
